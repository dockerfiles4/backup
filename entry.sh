#!bin/sh

echo "Starting container ..."

if [ -z ${RESTIC_REPOSITORY+x} ]; then
    echo "RESTIC_REPOSITORY env not set"
else
    restic snapshots &>/dev/null
    status=$?
    echo "Check repo status $status"

    if [ $status != 0 ]; then
        echo "Restic repository '${RESTIC_REPOSITORY}' does not exists. Running restic init."
        restic init

        init_status=$?
        echo "Repo init status $init_status"

        if [ $init_status != 0 ]; then
            echo "Failed to init the repository: '${RESTIC_REPOSITORY}'"
            exit 1
        fi
    fi

    echo "Setup backup cron job with cron expression BACKUP_CRON: ${BACKUP_CRON}"
    echo "${BACKUP_CRON} /bin/backup" > /var/spool/cron/crontabs/root

fi

crond -f -d 8

echo "Container started."
exec "$@"