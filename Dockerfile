FROM alpine:latest as restic
ARG RESTIC_VERSION=0.11.0
ADD https://github.com/restic/restic/releases/download/v${RESTIC_VERSION}/restic_${RESTIC_VERSION}_linux_amd64.bz2 /
RUN bzip2 -d restic_*_linux_amd64.bz2 && mv restic_*_linux_amd64 /bin/restic && chmod +x /bin/restic

FROM alpine:latest as rclone
ARG RCLONE_VERSION=1.53.3
ADD https://github.com/rclone/rclone/releases/download/v${RCLONE_VERSION}/rclone-v${RCLONE_VERSION}-linux-amd64.zip /
RUN unzip rclone-*-linux-amd64.zip && mv rclone-*-linux-amd64/rclone /bin/rclone 

FROM alpine:latest as timezone
ENV TZ="Europe/Brussels"
RUN apk add tzdata \
    && cp /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone

FROM alpine:latest
COPY --from=restic /bin/restic /bin/restic
COPY --from=rclone /bin/rclone /bin/rclone
COPY --from=timezone /etc/localtime /etc/timezone /etc/

ENV RESTIC_REPOSITORY=""
ENV RESTIC_PASSWORD=""
ENV BACKUP_CRON="0 */6 * * *"
ENV RESTIC_FORGET_ARGS=""
ENV RESTIC_JOB_ARGS=""

VOLUME ["/data", "/root/.config/rclone"]

COPY entry.sh /entry.sh
COPY backup.sh /bin/backup

WORKDIR /

CMD ["/entry.sh"]