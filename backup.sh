#!/bin/sh

start=`date +%s`
echo "Starting Backup at $(date +"%Y-%m-%d %H:%M:%S")"
echo "Starting Backup at $(date)"
echo "BACKUP_CRON: ${BACKUP_CRON}"
echo "RESTIC_FORGET_ARGS: ${RESTIC_FORGET_ARGS}"
echo "RESTIC_JOB_ARGS: ${RESTIC_JOB_ARGS}"
echo "RESTIC_REPOSITORY: ${RESTIC_REPOSITORY}"

restic backup /data ${RESTIC_JOB_ARGS}
rc=$?
echo "Finished backup at $(date)"
if [[ $rc == 0 ]]; then
    echo "Backup successfull" 
else
    echo "Backup failed with status ${rc}"
    restic unlock
    kill 1
fi

if [ -n "${RESTIC_FORGET_ARGS}" ]; then
    echo "Forget about old snapshots based on RESTIC_FORGET_ARGS = ${RESTIC_FORGET_ARGS}"
    restic forget ${RESTIC_FORGET_ARGS}
    rc=$?
    echo "Finished forget at $(date)"
    if [[ $rc == 0 ]]; then
        echo "Forget successfull"
    else
        echo "Forget failed with status ${rc}"
        restic unlock
    fi
fi

end=`date +%s`
echo "Finished backup at $(date +"%Y-%m-%d %H:%M:%S") after $((end-start)) seconds"